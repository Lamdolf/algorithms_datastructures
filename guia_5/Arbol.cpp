#include <iostream>
#include "Arbol.h"

using namespace std;


Arbol::Arbol(){}

void Arbol::add_elemento(Nodo *nodo, bool crece, int numero){

    Nodo *nodo1 = NULL;
    Nodo *nodo2 = NULL;


    if(nodo != NULL){

        if(numero < nodo->numero){
            add_elemento(nodo->izquierda, crece, numero);

            if(crece){
                if(nodo->equilibrio == 1){
                    nodo->equilibrio = 0;
                    crece = 0;
                }
                else if(nodo->equilibrio == 0){
                    nodo->equilibrio = -1;
                }
                else if(nodo->equilibrio == -1){
                    nodo1 = nodo->izquierda;

                    if(nodo1->equilibrio <= 0){
                        nodo->izquierda = nodo1->derecha;
                        nodo1->derecha = nodo;
                        nodo->equilibrio = 0;
                        nodo = nodo1;
                    }
                    else{
                        nodo2 = nodo1->derecha;
                        nodo->izquierda = nodo2->derecha;
                        nodo2->derecha = nodo;
                        nodo1->derecha = nodo2->izquierda;
                        nodo2->izquierda = nodo1;

                        if(nodo2->equilibrio == -1){
                            nodo->equilibrio = 1;
                        }
                        else{
                            nodo->equilibrio = 0;
                        }
                        if(nodo2->equilibrio == 1){
                            nodo1->equilibrio = -1;
                        }
                        else{
                            nodo1->equilibrio = 0;
                        }
                        nodo = nodo2;
                    }
                    nodo->equilibrio = 0;
                    crece = 0;
                }
            }
        }
        else if(numero > nodo->numero){
            add_elemento(nodo->derecha, crece, numero);

            if(crece){
                if(nodo->equilibrio == -1){
                    nodo->equilibrio == 0;
                    crece = 0;
                }
                else if(nodo->equilibrio == 0){
                    nodo->equilibrio = 1;
                }

                else if(nodo->equilibrio == 1){
                    nodo1 = nodo->derecha;

                    if(nodo1->equilibrio >= 0){
                        nodo->derecha = nodo1->izquierda;
                        nodo1->izquierda = nodo;
                        nodo->equilibrio = 0;
                        nodo = nodo1;
                    }
                    else{
                        nodo2 = nodo1->izquierda;
                        nodo->derecha = nodo2->izquierda;
                        nodo2->izquierda = nodo;
                        nodo1->izquierda = nodo2->derecha;
                        nodo2->derecha = nodo1;

                        if(nodo2->equilibrio == 1){
                            nodo->equilibrio = -1;
                        }
                        else{
                            nodo->equilibrio = 0;
                        }

                        if(nodo2->equilibrio == -1){
                            nodo1->equilibrio = 1;
                        }
                        else{
                            nodo1->equilibrio = 0;
                        }

                        nodo = nodo2;
                    }
                    nodo->equilibrio = 0;
                    crece = 0;
                }
            }

            else{
                cout << "La informacion ya se encuentra en el arbol" << endl;
            }

        }
    }
    else{
        nodo1 = new Nodo();
        nodo1->numero = numero;
        nodo1->equilibrio = 0;
        crece = 1;

        if(this->raiz == NULL){
            this->raiz = nodo1;
        }

        cout << "Elemento añadido con exito" << endl;
    }
}

// Función que retorna la raiz del árbol
Nodo *Arbol::get_raiz(){
    return this->raiz;
}
