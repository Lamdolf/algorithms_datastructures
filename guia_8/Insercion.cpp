#include <iostream>
#include <time.h>
#include "Insercion.h"

using namespace std;


Insercion::Insercion(){}


double Insercion::ordenar_insercion(int *array, int n){

    clock_t inicio = clock();
    int aux;
    int k;

    for(int i=1; i<n; i++){

        aux = array[i];
        k = i-1;

        while(k>=0 && aux < array[k]){
            array[k+1] = array[k];
            k--;
        }

        array[k+1] = aux;
    }
    return (double)(clock()-inicio)/CLOCKS_PER_SEC;
}


double Insercion::ordenar_insercion_binaria(int *array, int n){

    clock_t inicio = clock();
    int aux;
    int izq;
    int der;
    int m;
    int j;

    for(int i=1; i<n; i++){

        aux = array[i];
        izq = 0;
        der = i-1;

        while(izq <= der){
            m = (izq + der) / 2;

            if(aux <= array[m]){
                der = m-1;
            }
            else{
                izq = m+1;
            }
        }

        j = i-1;

        while(j >= izq){
            array[j+1] = array[j];
            j--;
        }

        array[izq] = aux;
    }
    return (double)(clock()-inicio)/CLOCKS_PER_SEC;
}


double Insercion::ordenar_shell(int *array, int n){

    clock_t inicio = clock();
    int aux;
    int aux1;
    int i;
    bool sorting;

    aux1 = n;
    while(aux1 > 0){
        aux1 = aux1/2;
        sorting = 1;

        while(sorting){
            sorting = 0;
            i = 0;

            while((i+aux1) < n){

                if(array[i] > array[i+aux1]){
                    aux = array[i];
                    array[i] = array[i+aux1];
                    array[i+aux1] = aux;
                    sorting = 1;
                }
                i++;
            }
        }
    }
    return (double)(clock()-inicio)/CLOCKS_PER_SEC;
}
