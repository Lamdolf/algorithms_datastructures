#include <iostream>
#include <time.h>
#include "Seleccion.h"

using namespace std;


Seleccion::Seleccion(){}


double Seleccion::ordenar_seleccion(int *array, int n){

    clock_t inicio = clock();
    int menor;
    int k;

    for(int i=0; i<=(n-1); i++){

        menor = array[i];
        k = i;

        for(int j=(i+1); j<n; j++){
            if(array[j] < menor){
                menor = array[j];
                k = j;
            }
        }

        array[k] = array[i];
        array[i] = menor;
    }
    return (double)(clock()-inicio)/CLOCKS_PER_SEC;
}
