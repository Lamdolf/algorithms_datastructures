#include <iostream>
#include <time.h>
#include "Burbuja.h"

using namespace std;

// Constructor por defecto
Burbuja::Burbuja(){}


// Función de ordenamiento por burbuja menor
double Burbuja::burbuja_menor(int *array, int n){

    clock_t inicio = clock();
    int aux;


    // El primer ciclo va "de atrás hacia adelante", mientras que el
    // segundo va de "adelante hacia atrás" moviendo el elemento menor
    // hacia la izquierda del arreglo
    for(int i=1; i<=n; i++){
        for(int j=(n-1); j>=i; j--){

            if(array[j-1] > array[j]){
                aux = array[j-1];
                array[j-1] = array[j];
                array[j] = aux;
            }
        }
    }

    return (double)(clock()-inicio)/CLOCKS_PER_SEC;
}


// Función de ordenamiento por burbuja mayor
double Burbuja::burbuja_mayor(int *array, int n){

    clock_t inicio = clock();
    int aux;

    // El primer ciclo va "de adelante hacia atrás", mientras que el
    // segundo va de "atrás hacia adelante" moviendo el elemento mayor
    // hacia la derecha del arreglo (es el inverso de burbuja menor pero ambos
    // ordenan de menor a mayor)
    for(int i=(n-2); i>=0; i--){
        for(int j=0; j<=i; j++){

            if(array[j] > array[j+1]){
                aux = array[j];
                array[j] = array[j+1];
                array[j+1] = aux;
            }
        }
    }

    return (double)(clock()-inicio)/CLOCKS_PER_SEC;
}
