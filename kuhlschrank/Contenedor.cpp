#include <iostream>
#include "Contenedor.h"

using namespace std;


Contenedor::Contenedor(){}

Contenedor::Contenedor(string empresa, int numero){

    this->empresa = empresa;
    this->numero = numero;
}

void Contenedor::set_empresa(string empresa){
    this->empresa = empresa;
}

void Contenedor::set_numero(int numero){
    this->numero = numero;
}

string Contenedor::get_empresa(){
    return this->empresa;
}

int Contenedor::get_numero(){
    return this->numero;
}
