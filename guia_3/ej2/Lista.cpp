#include <iostream>
#include "Lista.h"

using namespace std;

// Constructor por defecto de la clase "Lista"
Lista::Lista() {}

// Función que permite la creación y enlazado de nodos
void Lista::crear(int numero){

    Nodo *temp = new Nodo;
    temp->numero = numero;

    // En caso de que no existan nodos en la lista, el primero y a la vez el
    // último va a ser el único nodo existente
    if (this->primero == NULL){
        this->primero = temp;
        this->ultimo = temp;
    }

    // En caso de que si exista al menos un elemento en la lista, se pasa a
    // comprobar la posición en que deberá ir el nodo
    else{

        // Se instancia una variable que permita recorrer la lista de nodos
        Nodo *actual = this->primero;

        // Mientras existan nodos en la lista, se comprobará la posición en
        // la que debe ir el nodo
        while(actual != NULL){

            // Si el valor del nodo es menor que el primer elemento en la
            // lista, el primer elemento pasa a ser el nuevo nodo y se enlaza
            // con el anterior primer elemento
            if (temp->numero <= actual->numero){
                this->primero = temp;
                temp->sig = actual;
                break;
            }
            // Si el valor es mayor que el último de la lista, se coloca el
            // nodo al final de la lista y se enlaza como debe
            else if (temp->numero > this->ultimo->numero){
                this->ultimo->sig = temp;
                this->ultimo = temp;
                break;
            }
            // Si el valor del nodo es mayor o igual al elemento actual, o
            // menor o igual al elemento siguiente, se cambian los respectivos
            // punteros y se inserta el nodo
            else if (temp->numero >= actual->numero && temp->numero <= actual->sig->numero){
                temp->sig = actual->sig;
                actual->sig = temp;
                break;
            }


            // Si no se cumple ninguna de las anteriores, se pasa al siguiente
            // nodo de la lista para hacer comparaciones
            actual = actual->sig;
        }
    }
}

// Función que imprime la lista de menor a mayor
void Lista::imprimir(){

    // Se instancia una variable auxiliar para recorrer la lista sin perder
    // los valores de la lista
    Nodo *aux = this->primero;

    // Siempre que exista un nodo (el valor del actual no sea NULL), se irá
    // imprimiendo sus valores
    while (aux != NULL){
        cout << "(" << aux->numero << ")->";
        aux = aux->sig;
    }
    // Para representar el fin de la lista, se imprime la palabra FIN
    cout << "FIN" << endl << endl;
}

// Función que permite obtener el primero nodo de la lista, para asi recorrer
// toda la lista en función principal
Nodo *Lista::get_primero(){
    return this->primero;
}
