#include <iostream>
#include <stdlib.h>
#include "Universidad.h"

using namespace std;



int main(){

    string profesores;
    Universidad utal = Universidad();

    cout << "¿Cuantos profesores desea ingresar?" << endl;
    getline(cin, profesores);

    utal.add_profesores(stoi(profesores));

    return 0;
}
