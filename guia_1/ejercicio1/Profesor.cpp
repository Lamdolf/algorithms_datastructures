#include <iostream>
#include "Profesor.h"

using namespace std;


Profesor::Profesor(){
}

void Profesor::set_nombre(string nombre){
    this->nombre = nombre;
}

void Profesor::set_sexo(string sexo){

    if(sexo == "m"){
        this->sexo = "masculino";
    }
    else{
        this->sexo = "femenino";
    }
}

void Profesor::set_edad(int edad){
    this->edad = edad;
}

string Profesor::get_nombre(){
    return this->nombre;
}

string Profesor::get_sexo(){
    return this->sexo;
}

int Profesor::get_edad(){
    return this->edad;
}
