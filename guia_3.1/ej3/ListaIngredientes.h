#include <iostream>
#include "Ingredientes.h"

using namespace std;


#ifndef LISTAINGREDIENTES_H
#define LISTAINGREDIENTES_H


class ListaIngredientes{

    // Atributos que permiten recorrer la lista desde el inicio y añadir elementos
    // al final de la lista
    private:
        Ingrediente *primero = NULL;
        Ingrediente *ultimo = NULL;

    public:
        // Constructor por defecto
        ListaIngredientes();
        // Función para añadir ingredientes a la lista
        void add_ingrediente(string nombre);
        // Función para eliminar ingredientes de la lista
        void eliminar_ingrediente(int posicion);
        // Función que imprime la lista de ingredientes
        void imprimir_ingredientes();
};
#endif
