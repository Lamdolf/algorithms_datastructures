#include <iostream>
#include <stdlib.h>
#include "Lista.h"


using namespace std;

// Función que permite seleccionar postres
void seleccionar(Lista &postres){

    string opcion;

    // Se imprime lista de postres y se pregunta el número de postre que desea
    // seleccionar para trabajar con sus ingredientes
    cout << endl;
    postres.imprimir();

    cout << "Ingrese el número del postre que desea seleccionar: ";
    getline(cin, opcion);

    // Se llama a función y se le envía número de postre para ver si existe
    postres.seleccionar_nodo(stoi(opcion));
}


// Función que permite la eliminación de un determinado postre
void eliminar(Lista &lista){

    string opcion;

    // Se imprime la lista y se pregunta el número de postre que se desea eliminar
    lista.imprimir();

    cout << "Ingrese el número del postre que desea eliminar:" << endl;
    getline(cin, opcion);

    // Se llama a función de lista y se envía número de postre para ver si existe
    // y eliminarlo en caso de existir
    lista.eliminar_nodo(stoi(opcion));
    system("sleep 1");
}


// Función que permite añadir postres de forma ordenada alfabéticamente
void add_ordenado(Lista &lista){

    string nombre;

    // Se pregunta el nombre y se manda a función que ingresa el postre de
    // forma ordenada a la lista
    cout << "Ingrese nombre para su postre: ";
    getline(cin, nombre);

    lista.crear_ordenados(nombre);
    cout << "Postre añadido con éxito!" << endl;
    system("sleep 1");
}


// Función menu principal
void menu(Lista &postres){

    string opcion;
    string aux;

    while(1){


        cout << "¿Que desea hacer?" << endl << endl;
        cout << "1.- Visualizar lista de postres" << endl;
        cout << "2.- Añadir postre"<< endl;
        cout << "3.- Eliminar postre"<< endl;
        cout << "4.- Seleccionar postre"<< endl;
        cout << "5.- Salir"<< endl;
        getline(cin, opcion);

        // Si se selecciona la opción 1, se imprime la lista de postres actual
        if (opcion == "1"){
            cout << endl;
            postres.imprimir();
            cout << endl << "Presione enter para continuar..." << endl;
            getline(cin, aux);
        }

        // Si se selecciona opción 2, se llama a función para añadir postres
        else if (opcion == "2"){
            add_ordenado(postres);
        }

        // Si se selecciona opción 3, se llama a función para eliminar postres
        else if (opcion == "3"){
            eliminar(postres);
        }

        // Si se selecciona opción 4, se llama a función para seleccionar un
        // postre y trabajar con sus ingredientes
        else if (opcion == "4"){
            seleccionar(postres);
        }

        // Opción 5 termina bucle de menu
        else if (opcion == "5"){
            break;
        }

        else{
            cout << "Opción no válida" << endl;
            system("sleep 1");
        }

        system("clear");
    }
}


// Función principal
int main(){

    Lista postres;

    // Se llama a menu de opciones
    menu(postres);

    return 0;
}
