#include <iostream>
#include "Lista.h"

using namespace std;


// Función para agregar nombres a la lista
void llenar_lista(Lista &nombres){

    string cantidad;
    string nombre;

    // Se pregunta cuantos nombres se van a ingresar
    cout << "¿Cuántos nombres va a ingresar?" << endl;
    getline(cin, cantidad);

    // Con ayuda de bucle for, se añaden nombres de forma ordenada y además
    // se imprime la lista actual de elementos
    for (int i=0; i<stoi(cantidad); i++){

        cout << "Ingrese nombre:";
        getline(cin, nombre);
        nombres.crear_ordenados(nombre);
        nombres.imprimir();
    }
}

// Función principal
int main(){

    Lista nombres;

    // Se llama a función que permite ingreso de nombres
    llenar_lista(nombres);

    // Se imprime lista final de nombres
    cout << "Lista final:" << endl << endl;
    nombres.imprimir();

    return 0;
}
