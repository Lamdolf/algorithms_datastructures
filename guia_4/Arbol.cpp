#include <iostream>
#include <stdlib.h>
#include "Arbol.h"

using namespace std;

// Constructor por defecto
Arbol::Arbol(){}


// Función que añade elementos al árbol
void Arbol::add_elemento(Nodo *nodo, int numero){

    // Si la raiz es null, el árbol esta vacío, por lo que se añade el número
    // al inicio del árbol
    if (this->raiz == NULL){
        Nodo *temp = new Nodo();
        temp->numero = numero;
        this->raiz = temp;
        cout << "Elemento añadido con éxito!";
    }

    // Si existe al menos un elemento en el árbol, se busca donde corresponde el nuevo nodo
    else{
        // Si el elemento es menor, se revisa la izquierda
        if(numero < nodo->numero){

            // Si a la izquierda no hay nada, se inserta el nodo
            if(nodo->izquierda == NULL){
                Nodo *temp = new Nodo();
                temp->numero = numero;
                nodo->izquierda = temp;
                cout << "Elemento añadido con éxito!";
            }
            // Si hay algo, se revisa aquel nodo por su izquierda y derecha
            else{
                add_elemento(nodo->izquierda, numero);
            }
        }

        // Si el elemento es mayor se revisa la derecha
        else if (numero > nodo->numero){

            // Si no hay nada a la derecha, se inserta el nodo
            if(nodo->derecha == NULL){
                Nodo *temp = new Nodo();
                temp->numero = numero;
                nodo->derecha = temp;
                cout << "Elemento añadido con éxito!";
            }
            // Sino, se sigue revisando el nodo de la derecha
            else{
                add_elemento(nodo->derecha, numero);
            }
        }

        // Si no es ni mayor ni menor, es igual, por lo que no se añade al árbol
        else{
            cout << "El número ingresado ya se encuentra en el árbol" << endl;
        }
    }
}


// Función que retorna la raiz del árbol
Nodo *Arbol::get_raiz(){
    return this->raiz;
}
