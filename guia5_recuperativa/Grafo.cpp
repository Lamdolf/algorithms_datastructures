#include <iostream>
#include <fstream>
#include "Grafo.h"

using namespace std;

// Constructor por defecto
Grafo::Grafo(){}


// Función que da inicio a la generación de grafo
void Grafo::generar_grafo(Nodo *nodo){

    ofstream archivo;

    // Se abre archivo temporal
    archivo.open("grafo.temp");

    // Lineas inciales de archivo
    archivo << "digraph G{" << endl;
    archivo << "node [style=filled shape=octagon fillcolor=darkorchid1]" << endl;
    archivo << "null1 [shape=point]" << endl;
    archivo << "null1->" << nodo->numero << " [label=" << nodo->equilibrio << "]" << endl;

    // Se comienza a recorrer el arbol desde la raiz
    recorrer(nodo, archivo);

    // Linea final del archivo
    archivo << "}" << endl;
    archivo.close();

    // Despues de crear el archivo, se general el grafo, se borra el archivo temporal
    // y se abre el grafo correspondiente
    system("dot -Tpng -o grafo.png grafo.temp");
    system("rm grafo.temp");
    system("eog grafo.png");
}


// Función recursiva para recorrer árbol
void Grafo::recorrer(Nodo *nodo, ofstream &archivo){

    // Si existe un elemento en el árbol se escribirá el archivo
    if (nodo != NULL){

        // Si a la izquierda hay algo, se conecta en el grafo con el nodo
        if (nodo->izquierda != NULL){
            archivo << nodo->numero << " -> " << nodo->izquierda->numero << " [arrowshape=vee]"
                    << "[label=" << nodo->izquierda->equilibrio << "]" << endl;
        }
        // Si no hay nada, se conecta a un punto vacío
        else{
            archivo << "i" << nodo->numero << " [shape=point]" << endl;
            archivo << nodo->numero << " -> " << "i" << nodo->numero  << endl;
        }

        // Si a la derecha hay algo, se conecta el grafo con el siguiente nodo
        if (nodo->derecha != NULL){
            archivo << nodo->numero << " -> " << nodo->derecha->numero << " [arrowshape=vee]"
                    << "[label=" << nodo->derecha->equilibrio << "]" << endl;
        }
        // Sino, se conecta a un punto vacio
        else{
            archivo << "d" << nodo->numero << " [shape=point]" << endl;
            archivo << nodo->numero << " -> " << "d" << nodo->numero << endl;
        }

        // Se llama nuevamente a la función siguiendo por la izquierda y por la derecha
        recorrer(nodo->derecha, archivo);
        recorrer(nodo->izquierda, archivo);
    }


}
