# Guia5_recuperativa

Guía 5 - Creación de Arboles Balanceados AVL

## Requisitos previos

Para la completa ejecución del programa es necesario contar con compilador de Gcc y con el paquete Graphviz para la generación de grafos, este último puede ser instalado ejecutando el siguiente comando en una terminal:

>sudo apt-get install graphviz

---

## Compilación y ejecución

Ya teniendo instalado un compilador de gcc compatible con c++, la compilación del programa se realiza ejecutando el comando "make" desde una terminal. La ejecución del programa se realiza con el comando "./Main" sin añadir ningún parámetro. Un ejemplo de compilación y ejecución es el siguiente:

>make  
>./Main

---

## Sobre el programa

El presente programa permite la creación de un árbol balanceado AVL con todas las reglas que este implica, al ejecutarse se le presentará al usuario un menú básico con las siguientes opciones:
* Añadir un número al árbol
* Eliminar un número del árbol
* Modificar un valor del árbol
* Generar el grafo correspondiente al árbol
* Salir

Si el usuario elige añadir un número, se le preguntará por el número que desea añadir, este sólo se añadirá si no se encuentra ya en el árbol (no se permiten valores repetidos)

Si el usuario quiere eliminar un número, se pregunta cuál número desea borrar, este sólo se borra si efectivamente existe en el árbol.

Si se quiere modificar un valor, se pregunta por el valor que se quiere cambiar y que valor nuevo quiere que tome, antes de realizar cualquier cambio se comprueba que:
* El valor a cambiar se encuentre en el árbol
* El nuevo valor a tomar NO se encuentre en el árbol

Si se elige generar grafo correspondiente, se genera internamente el grafo con uso del paquete graphviz y se abre la imagen generada.

La opción salir termina el programa con código de salida 0 (correcta ejecución).

---

## Sobre el balance del árbol

Que el árbol sea balanceado significa que ambos entre una rama y otra de un nodo, no puede haber una diferencia de largo de mas de 1 elemento, si una rama tiene una altura de dos niveles de diferencia con otra, se realizan reestructuraciones en el árbol para que este se mantendrá de cierta forma balanceado o ligeramente simétrico (reduciendo así tiempos de búsqueda de elementos en su interior).

---

# Desarrollo

Implementación por Arturo Lobos  
Editor Atom  
Ubuntu 18.04.03 LTS  
gcc 7.5.0  

---

# Referencias
* Estructuras de Datos, Osvaldo Cairó, Tercera Edición, Capı́tulo 6.4, P.214-241
