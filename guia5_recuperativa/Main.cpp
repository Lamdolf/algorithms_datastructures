#include <iostream>
#include <stdlib.h>
#include "Arbol.h"
#include "Grafo.h"

using namespace std;



// Funcion con menu principal
void menu(){

    string opcion;
    string numero;
    string numero1;
    Arbol avl;
    Grafo grafo;
    bool crece = 0;


    // Bucle infinito que mantiene funcionando el programa hasta que se
    // elija terminarlo
    while(1){

        cout << "+-------------------------------------------+" << endl;
        cout << "|             ÁRBOL BINARIO AVL             |" << endl;
        cout << "+-------------------------------------------+" << endl << endl;

        cout << "¿Qué desea hacer?" << endl << endl;

        cout << "1.- Ingresar número al árbol" << endl;
        cout << "2.- Eliminar un número del árbol" << endl;
        cout << "3.- Modificar un válor del árbol" << endl;
        cout << "4.- Generar grafo correspondiente al árbol" << endl;
        cout << "5.- Salir" << endl << endl;

        getline(cin, opcion);

        // Si el usuario quiere ingresar un número, se lee y se envía a
        // método de clase árbol que verifica y añade el número
        if(opcion == "1"){
            cout << "Ingrese número para añadir al árbol: ";
            getline(cin, numero);
            avl.insertar_numero(avl.get_raiz(), crece, stoi(numero));
        }

        // Si se quiere eliminar un número, se lee y se llama a método
        // que buscará y eliminará el número
        else if(opcion == "2"){
            cout << "Ingrese número que quiere eliminar del árbol: ";
            getline(cin, numero);
            avl.eliminar_numero(avl.get_raiz(), crece, stoi(numero));
        }

        // En caso de querer modificar un número, se pregunta que número
        // quiere cambiar, y que valor quiere poner en su lugar
        else if(opcion == "3"){

            cout << "¿Que número desea modificar?: ";
            getline(cin, numero);
            cout << "¿Por cual número quiere reemplazarlo?: ";
            getline(cin, numero1);

            // Antes de cualquier operación, se verifica que el número que
            // se quiere modificar este efectivamente en el árbol, y que además
            // el nuevo valor NO este en el árbol
            if((!avl.en_arbol(stoi(numero))) && avl.en_arbol(stoi(numero1))){
                cout << "El número que quiere modificar no existe "
                     << "y lo quiere modificar por un número que ya está en el árbol" << endl;
                system("sleep 1");

            }
            else if(avl.en_arbol(stoi(numero1))){
                cout << "El número por el cual quiere cambiar el anterior ya se encuentra en el árbol" << endl;
            }
            else if(!avl.en_arbol(stoi(numero))){
                cout << "El número que quiere modificar no existe" << endl;
            }

            // Si ambos valores son válidos, se procede a eliminar uno y
            // añadir el otro
            else{
                avl.eliminar_numero(avl.get_raiz(), crece, stoi(numero));
                crece = 0;
                avl.insertar_numero(avl.get_raiz(), crece, stoi(numero1));
                cout << "Modificación realizada con éxito!" << endl;
            }

        }

        // Si se elige la opción de generar grafo, se llama al método de la
        // clase grafo, pasandole como parámetro, la raiz del árbol
        else if(opcion == "4"){
            grafo.generar_grafo(avl.get_raiz());
        }

        // En caso de querer salir, se sale del programa con código 0
        else if(opcion == "5"){
            cout << "Auf wiedersehen!" << endl;
            exit(0);
        }

        // Cualquier otro dígito ingresado no será valido
        else{
            cout << "La opción ingresada no es válida" << endl;
            system("sleep 1");
            system("clear");
        }

        // Se espera un poco antes de reiniciar el menu, para alcanzar a leer
        crece = 0;
        system("sleep 1");
        system("clear");
    }

}



// Función Main
int main(int argc, char **argv){

    // Se verifica que no se hayan ingresado parámetros al iniciar
    if(argc != 1){
        cout << "Error, no se deben ingrear parámetros" << endl;
    }
    else{
        menu();
    }

    return 0;
}
