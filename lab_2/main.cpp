#include <iostream>
#include <stack>
#include "Pila.h"


using namespace std;


int main(){

    string largo;

    cout << "¿Cual será el largo de su pila?" << endl;
    getline(cin, largo);

    Pila pila = Pila(stoi(largo));

    pila.menu();

    return 0;
}
