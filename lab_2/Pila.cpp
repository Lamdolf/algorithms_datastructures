#include <iostream>
#include <stack>
#include <stdlib.h>
#include "Pila.h"
#include "Operador.h"

using namespace std;

Pila::Pila(int n){

    this->largo = n;
    this->tope = 0;
    this->op = Operador();
}

void Pila::menu(){

    string opcion;
    string aux;

    for(;;){

        cout << "¿Que desea hacer?" << endl;
        cout << "1.- Imprimir pila" << endl;
        cout << "2.- Añadir elemento a la pila" << endl;
        cout << "3.- Quitar elemento de la pila" << endl;
        cout << "4.- Vaciar pila" << endl;
        cout << "5.- Salir" << endl;

        getline(cin, opcion);

        if(opcion == "1"){

            if(op.pila_vacia(this->tope)){
                cout << "Su pila está actualmente vacía..." << endl;
                system("sleep 1.5");
            }
            else{
                op.revisar_pila(this->tope, this->pila_numeros);
                getline(cin, aux);
            }
        }

        else if(opcion == "2"){

            if(op.pila_llena(this->tope, this->largo)){
                cout << "No se pueden agregar mas elementos, la pila está llena"
                     << endl;
                system("sleep 1.5");
            }
            else{
                op.add_elemento(this->tope, this->largo, this->pila_numeros);
                cout << "Se han añadido todos los elementos correctamente, presione enter para continuar"
                     << endl;
                this->tope = pila_numeros.size();

                getline(cin, aux);
            }
        }

        else if(opcion == "3" || opcion == "4"){

            if(op.pila_vacia(this->tope)){
                cout << "Su pila está actualmente vacía..." << endl;
            }
            else{
                if (opcion == "3"){
                    op.quitar_elemento(this->pila_numeros, 0);
                    cout << "Se ha quitado un elemento de la lista" << endl;
                }
                else{
                    op.quitar_elemento(this->pila_numeros, 1);
                    cout << "Se ha vaciado la lista" << endl;
                }
                this->tope = this->pila_numeros.size();
            }
            system("sleep 1.5");
        }

        else if(opcion == "5"){
            break;
        }

        else{
            cout << "Opción no válida >:C";
            system("sleep 1.5");
        }

        system("clear");
    }
}
